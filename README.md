# 代码片段

> 分享代码片段。有access 帮助类，sqlsugar 帮助类等。

## AccessHelper
access数据库帮助类， crud方法，含有分页方法。


## DataTableExtensions
DataTable与List 相互转换扩展方法

## Email
简易的邮件发送工具类


## HttpHelper
http请求相关 获取ip和域名等相关的工具类


## ImageWaterMark

图片水印帮助类，可以添加图片水印和文字水印。

## INIHelper
ini文件读写工具类


## KindeditorHelper
kindeditor 富文本编辑器帮助类，实现多种模式配置展示不同效果。适用于webform mvc方式。

在js/kindeditor-all.js    v4.1.12  带复制粘贴上传图片功能

## LambdaHelper
拼接lambda工具类

## LogHelper
简单的txt日志工具类，不依赖于任何框架，适用于低版本的framework简单场景。


## NPOIHelper

NPOIHelper 帮助类，可以实现DataTable或DataReader导入到excel文件直接下载；或将excel数据导入到DataTable中。
NPOIHelper_html 帮助类，excel转html


## ObjectMethodExtensions
深度克隆 深度复制 工具类。


## Security
加密解密相关安全的工具类, 有
* MD5
* AES加密解密,和java AES通用
* 对称加密算法AES RijndaelManaged加密解密 
* DES对称加密解密 
* Base64 
* Base62 
* SHA256加密算法 
* RSA加密 解密 
* RC4加密 解密
* 十进制数和64进制数相互转换

## SqlServerHelper
SqlServer Sql帮助类和扩展方法，包含分页方法和动态条件。


## SqlSugarExtensions
基于sqlsugar orm，自己封装的简易扩展方法类。代码末尾含有使用示例。

## TimeHelper
时间日期显示相关工具类，包括时间戳转换方法

## TxtHelper
txt文本读写工具类

## UploadFileCommand
上传帮助类，支持webform和mvc方式上传。

## XmlConvert
xml序列化转化类

## VerificationCode
生成验证码工具类

## ZipHelper
压缩解压缩工具类 基于SharpZipLib


# FreeSql相关代码片段

## FreeSqlHelper
基于freesql orm，自己简单封装的方法实现单表操作。代码末尾含有使用示例。

## FreeSqlSetup
freesql aop设置。

## IdleBusExtesions
freesql多数据库使用帮助类

## SnowflakEntity
雪花实体示例，配合FreeSqlHelper使用

## Snowflake
雪花id算法帮助类。



# js代码片段

## kindeditor-all.js
kindeditor v 4.1.12 带复制粘贴上传图片功能

## posfixed.js 
//改写有bug的版https://www.dowebok.com/demo/2013/21/
//调用方式
```
 $(function() {
            $("#bottomDiv").posfixed({ distance: 0, direction: "bottom", type: "always", hide: false });
        })
```
//direction: top或bottom 
//distance:离顶部或底部的数值
//type: while 滚动到distance数值固定，always一直固定
//hide:bool值 是否自动隐藏对象

(额外扩展一下，可以用纯css方式实现吸顶效果  position:fixed;top:0px;z-index:999 ,可能有些浏览器不支持)