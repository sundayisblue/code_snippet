﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace BoYuan.Framework.Uitility
{

    public class ImageWaterMark
    {
        /// <summary>
        /// 图片水印
        /// </summary>
        /// <param name="imgUrl">服务器图片相对路径</param>
        /// <param name="filenameUrl">保存文件名</param>
        /// <param name="watermarkFilename">水印文件相对路径</param>
        /// <param name="watermarkStatus">图片水印位置 0=不使用 1=左上 2=中上 3=右上 4=左中  9=右下</param>
        /// <param name="quality">附加水印图片质量,0-100</param>
        /// <param name="watermarkTransparency">水印的透明度 1--10 10为不透明</param>
        public static void AddImageSignPic(string imgUrl, string filenameUrl, string watermarkFilename, int watermarkStatus, int quality, int watermarkTransparency)
        {
            byte[] _ImageBytes = File.ReadAllBytes(GetMapPath(imgUrl));
            Image img = Image.FromStream(new System.IO.MemoryStream(_ImageBytes));
            filenameUrl = HttpContext.Current.Server.MapPath(filenameUrl);

            //if (watermarkFilename.StartsWith("/") == false)
            //    watermarkFilename = "/" + watermarkFilename;
            watermarkFilename = GetMapPath(watermarkFilename);
            if (!File.Exists(watermarkFilename))
                return;
            Graphics g = Graphics.FromImage(img);
            //设置高质量插值法
            //g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            //设置高质量,低速度呈现平滑程度
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Image watermark = new Bitmap(watermarkFilename);

            //if (watermark.Height >= img.Height || watermark.Width >= img.Width)
            //    return;

            ImageAttributes imageAttributes = new ImageAttributes();
            ColorMap colorMap = new ColorMap();

            colorMap.OldColor = Color.FromArgb(255, 0, 255, 0);
            colorMap.NewColor = Color.FromArgb(0, 0, 0, 0);
            ColorMap[] remapTable = { colorMap };

            imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);

            float transparency = 0.5F;
            if (watermarkTransparency >= 1 && watermarkTransparency <= 10)
                transparency = (watermarkTransparency / 10.0F);


            float[][] colorMatrixElements = {
                                                new float[] {1.0f,  0.0f,  0.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  1.0f,  0.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  0.0f,  1.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  0.0f,  0.0f,  transparency, 0.0f},
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 1.0f}
                                            };

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

            imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            int xpos = 0;
            int ypos = 0;

            switch (watermarkStatus)
            {
                case 1:
                    xpos = (int)(img.Width * (float).01);
                    ypos = (int)(img.Height * (float).01);
                    break;
                case 2:
                    xpos = (int)((img.Width * (float).50) - (watermark.Width / 2));
                    ypos = (int)(img.Height * (float).01);
                    break;
                case 3:
                    xpos = (int)((img.Width * (float).99) - (watermark.Width));
                    ypos = (int)(img.Height * (float).01);
                    break;
                case 4:
                    xpos = (int)(img.Width * (float).01);
                    ypos = (int)((img.Height * (float).50) - (watermark.Height / 2));
                    break;
                case 5:
                    xpos = (int)((img.Width * (float).50) - (watermark.Width / 2));
                    ypos = (int)((img.Height * (float).50) - (watermark.Height / 2));
                    break;
                case 6:
                    xpos = (int)((img.Width * (float).99) - (watermark.Width));
                    ypos = (int)((img.Height * (float).50) - (watermark.Height / 2));
                    break;
                case 7:
                    xpos = (int)(img.Width * (float).01);
                    ypos = (int)((img.Height * (float).99) - watermark.Height);
                    break;
                case 8:
                    xpos = (int)((img.Width * (float).50) - (watermark.Width / 2));
                    ypos = (int)((img.Height * (float).99) - watermark.Height);
                    break;
                case 9:
                    xpos = (int)((img.Width * (float).99) - (watermark.Width));
                    ypos = (int)((img.Height * (float).99) - watermark.Height);
                    break;
            }

            g.DrawImage(watermark, new Rectangle(xpos, ypos, watermark.Width, watermark.Height), 0, 0, watermark.Width, watermark.Height, GraphicsUnit.Pixel, imageAttributes);

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType.IndexOf("jpeg") > -1)
                    ici = codec;
            }
            EncoderParameters encoderParams = new EncoderParameters();
            long[] qualityParam = new long[1];
            if (quality < 0 || quality > 100)
                quality = 80;

            qualityParam[0] = quality;

            EncoderParameter encoderParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityParam);
            encoderParams.Param[0] = encoderParam;

            if (ici != null)
                img.Save(filenameUrl, ici, encoderParams);
            else
                img.Save(filenameUrl);

            g.Dispose();
            img.Dispose();
            watermark.Dispose();
            imageAttributes.Dispose();
        }

        /// <summary>
        /// 图片水印，根据坐标值
        /// </summary>
        /// <param name="imgUrl">服务器图片相对路径</param>
        /// <param name="filenameUrl">保存文件名</param>
        /// <param name="watermark">水印图片文件</param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="quality">附加水印图片质量,0-100</param>
        /// <param name="watermarkTransparency">水印的透明度 1--10 10为不透明</param>
        public static void AddImageSignPicByXY(string imgUrl, string filenameUrl, Image watermark, int x, int y, int quality=100, int watermarkTransparency=10)
        {
            byte[] _ImageBytes = File.ReadAllBytes(GetMapPath(imgUrl));
            Image img = Image.FromStream(new System.IO.MemoryStream(_ImageBytes));
            filenameUrl = HttpContext.Current.Server.MapPath(filenameUrl);

            Graphics g = Graphics.FromImage(img);

            ImageAttributes imageAttributes = new ImageAttributes();
            ColorMap colorMap = new ColorMap();

            colorMap.OldColor = Color.FromArgb(255, 0, 255, 0);
            colorMap.NewColor = Color.FromArgb(0, 0, 0, 0);
            ColorMap[] remapTable = { colorMap };

            imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);

            float transparency = 0.5F;
            if (watermarkTransparency >= 1 && watermarkTransparency <= 10)
                transparency = (watermarkTransparency / 10.0F);


            float[][] colorMatrixElements = {
                                                new float[] {1.0f,  0.0f,  0.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  1.0f,  0.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  0.0f,  1.0f,  0.0f, 0.0f},
                                                new float[] {0.0f,  0.0f,  0.0f,  transparency, 0.0f},
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 1.0f}
                                            };

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

            imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            g.DrawImage(watermark, new Rectangle(x, y, watermark.Width, watermark.Height), 0, 0, watermark.Width, watermark.Height, GraphicsUnit.Pixel, imageAttributes);

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType.IndexOf("jpeg") > -1)
                    ici = codec;
            }
            EncoderParameters encoderParams = new EncoderParameters();
            long[] qualityParam = new long[1];
            if (quality < 0 || quality > 100)
                quality = 80;

            qualityParam[0] = quality;

            EncoderParameter encoderParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityParam);
            encoderParams.Param[0] = encoderParam;

            if (ici != null)
                img.Save(filenameUrl, ici, encoderParams);
            else
                img.Save(filenameUrl);

            g.Dispose();
            img.Dispose();
            watermark.Dispose();
            imageAttributes.Dispose();
        }

        /// <summary>
        /// 设置指定尺寸等比缩放图片
        /// </summary>
        /// <param name="sourceImageUrl">图片相对路径</param>
        /// <param name="targetWidth"></param>
        /// <param name="targetHeight"></param>
        /// <returns></returns>
        public static Image PictureProcess(string sourceImageUrl, int targetWidth, int targetHeight)
        {
            int width; //图片最终的宽
            int height; //图片最终的高
            try
            {

                Image sourceImage = new Bitmap(HttpContext.Current.Server.MapPath(sourceImageUrl));

                System.Drawing.Imaging.ImageFormat format = sourceImage.RawFormat;
                Bitmap targetPicture = new Bitmap(targetWidth, targetHeight);
                Graphics g = Graphics.FromImage(targetPicture);
                g.Clear(Color.White);

                //计算缩放图片的大小
                if (sourceImage.Width > targetWidth && sourceImage.Height <= targetHeight)
                {
                    width = targetWidth;
                    height = (width * sourceImage.Height) / sourceImage.Width;
                }
                else if (sourceImage.Width <= targetWidth && sourceImage.Height > targetHeight)
                {
                    height = targetHeight;
                    width = (height * sourceImage.Width) / sourceImage.Height;
                }
                else if (sourceImage.Width <= targetWidth && sourceImage.Height <= targetHeight)
                {
                    width = sourceImage.Width;
                    height = sourceImage.Height;
                }
                else
                {
                    width = targetWidth;
                    height = (width * sourceImage.Height) / sourceImage.Width;
                    if (height > targetHeight)
                    {
                        height = targetHeight;
                        width = (height * sourceImage.Width) / sourceImage.Height;
                    }
                }

                g.DrawImage(sourceImage, (targetWidth - width) / 2, (targetHeight - height) / 2, width, height);
                sourceImage.Dispose();

                return targetPicture;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        /// <summary>
        /// 批量设置文字水印
        /// </summary>
        /// <param name="imgUrl">服务器图片相对路径</param>
        /// <param name="filenameUrl">保存文件名</param>
        /// <param name="quality">附加水印图片质量,0-100</param>
        /// <param name="textList">水印文字相关信息</param>
        public static void AddImageText(string imgUrl, string filenameUrl, int quality, List<WaterMarkText> textList)
        {
            byte[] _ImageBytes = File.ReadAllBytes(HttpContext.Current.Server.MapPath(imgUrl));
            Image img = Image.FromStream(new System.IO.MemoryStream(_ImageBytes));
            filenameUrl = HttpContext.Current.Server.MapPath(filenameUrl);

            Graphics g = Graphics.FromImage(img);

            foreach (var text in textList)
            {
                g.DrawString(text.Text, text.FontInfo, text.SolidBrushInfo, text.X, text.Y);
            }


            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType.IndexOf("jpeg") > -1)
                    ici = codec;
            }
            EncoderParameters encoderParams = new EncoderParameters();
            long[] qualityParam = new long[1];
            if (quality < 0 || quality > 100)
                quality = 80;

            qualityParam[0] = quality;

            EncoderParameter encoderParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityParam);
            encoderParams.Param[0] = encoderParam;

            if (ici != null)
                img.Save(filenameUrl, ici, encoderParams);
            else
                img.Save(filenameUrl);

            g.Dispose();
            img.Dispose();
        }

        public class WaterMarkText
        {
            public WaterMarkText()
            {
                //默认值
                SolidBrushInfo = new SolidBrush(Color.Black);
                FontInfo = new Font("微软雅黑", 16, FontStyle.Regular, GraphicsUnit.Pixel);
            }

            /// <summary>
            /// 文字
            /// </summary>
            public string Text { get; set; }

            /// <summary>
            /// x坐标
            /// </summary>
            public float X { get; set; }

            /// <summary>
            /// y坐标
            /// </summary>
            public float Y { get; set; }

            /// <summary>
            /// 颜色信息
            /// </summary>
            public SolidBrush SolidBrushInfo { get; set; }

            /// <summary>
            /// 字体信息
            /// </summary>
            public Font FontInfo { get; set; }
        }


        /// <summary>
        /// 文字水印
        /// </summary>
        /// <param name="imgUrl">服务器图片相对路径</param>
        /// <param name="filenameUrl">保存文件名</param>
        /// <param name="watermarkText">水印文字</param>
        /// <param name="watermarkStatus">图片水印位置 0=不使用 1=左上 2=中上 3=右上 4=左中  9=右下</param>
        /// <param name="quality">附加水印图片质量,0-100</param>
        /// <param name="fontname">字体</param>
        /// <param name="fontsize">字体大小</param>
        public static void AddImageSignText(string imgUrl, string filenameUrl, string watermarkText, int watermarkStatus, int quality, string fontname, int fontsize)
        {
            byte[] _ImageBytes = File.ReadAllBytes(HttpContext.Current.Server.MapPath(imgUrl));
            Image img = Image.FromStream(new System.IO.MemoryStream(_ImageBytes));
            filenameUrl = HttpContext.Current.Server.MapPath(filenameUrl);

            Graphics g = Graphics.FromImage(img);
            Font drawFont = new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Pixel);
            SizeF crSize;
            crSize = g.MeasureString(watermarkText, drawFont);

            float xpos = 0;
            float ypos = 0;

            switch (watermarkStatus)
            {
                case 1:
                    xpos = (float)img.Width * (float).01;
                    ypos = (float)img.Height * (float).01;
                    break;
                case 2:
                    xpos = ((float)img.Width * (float).50) - (crSize.Width / 2);
                    ypos = (float)img.Height * (float).01;
                    break;
                case 3:
                    xpos = ((float)img.Width * (float).99) - crSize.Width;
                    ypos = (float)img.Height * (float).01;
                    break;
                case 4:
                    xpos = (float)img.Width * (float).01;
                    ypos = ((float)img.Height * (float).50) - (crSize.Height / 2);
                    break;
                case 5:
                    xpos = ((float)img.Width * (float).50) - (crSize.Width / 2);
                    ypos = ((float)img.Height * (float).50) - (crSize.Height / 2);
                    break;
                case 6:
                    xpos = ((float)img.Width * (float).99) - crSize.Width;
                    ypos = ((float)img.Height * (float).50) - (crSize.Height / 2);
                    break;
                case 7:
                    xpos = (float)img.Width * (float).01;
                    ypos = ((float)img.Height * (float).99) - crSize.Height;
                    break;
                case 8:
                    xpos = ((float)img.Width * (float).50) - (crSize.Width / 2);
                    ypos = ((float)img.Height * (float).99) - crSize.Height;
                    break;
                case 9:
                    xpos = ((float)img.Width * (float).99) - crSize.Width;
                    ypos = ((float)img.Height * (float).99) - crSize.Height;
                    break;
            }

            g.DrawString(watermarkText, drawFont, new SolidBrush(Color.White), xpos + 1, ypos + 1);
            g.DrawString(watermarkText, drawFont, new SolidBrush(Color.Black), xpos, ypos);

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType.IndexOf("jpeg") > -1)
                    ici = codec;
            }
            EncoderParameters encoderParams = new EncoderParameters();
            long[] qualityParam = new long[1];
            if (quality < 0 || quality > 100)
                quality = 80;

            qualityParam[0] = quality;

            EncoderParameter encoderParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityParam);
            encoderParams.Param[0] = encoderParam;

            if (ici != null)
                img.Save(filenameUrl, ici, encoderParams);
            else
                img.Save(filenameUrl);

            g.Dispose();
            img.Dispose();
        }

        public static string GetMapPath(string url)
        {
            if (url.ToLower().StartsWith("http://"))
            {
                return url;
            }
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Server.MapPath(url);
            }
            else//非web程序引用
            {
                url = url.Replace("/", "\\");
                if (url.StartsWith("\\"))
                {
                    url = url.Substring(url.IndexOf('\\', 1)).TrimStart('\\');
                }
                return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, url);
            }
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="url">相对路径</param>
        public static void DeleteFile(string url)
        {
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(url)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(url));
            }
        }



    }
}

/*
    string basepic = "/res/images/backPic.png";//背景图
    string newPicSaveUrl = "/res/images/newspic.jpg";//生成新图片地址

    //图片
    Bitmap wm = QrCodeHelper.ShowCode(F_Code.Text,int.Parse(F_qrSize.Text));//二维码图片
    ImageWaterMark.AddImageSignPicByXY(basepic, newPicSaveUrl, wm, int.Parse(F_qrX.Text),int.Parse(F_qrY.Text));

    //文字 批量打印
    List<ImageWaterMark.WaterMarkText> textList=new List<ImageWaterMark.WaterMarkText>
    {
        new ImageWaterMark.WaterMarkText(){ Text =RealName.Text,X=84,Y=242},
        new ImageWaterMark.WaterMarkText(){ Text =EmployeeCode.Text,X=84,Y=291}
    };
    ImageWaterMark.AddImageText(newPicSaveUrl, newPicSaveUrl,100,textList);
 */