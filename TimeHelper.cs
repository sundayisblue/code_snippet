using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYuan.Framework.Uitility
{
    public static class TimeHelper
    {

        /// <summary>
        /// 指定获取UTC时间的每天time字符串(例如"02:23:11")
        /// </summary>
        /// <param name="hour">北京时间小时数(0到23)</param>
        /// <param name="minute">北京时间分钟数(0到59)</param>
        /// <param name="second">北京时间秒数(0到59)</param>
        /// <remarks>utc和北京时间有8个小时时差，所以北京时间要减去8小时才是utc时间</remarks>
        /// <returns></returns>
        public static string GetUtcTimeDayStrByBeijingTime(int hour, int minute = 0, int second = 0)
        {
            //当前的北京时间。年月日不重要
            DateTime beijingTime = new DateTime(2000, 1, 1, hour, minute, second);

            //将北京时间转换为UTC时间
            DateTime utcTime = beijingTime.ToUniversalTime();
            return utcTime.ToString("HH:mm:ss");
        }

        /// <summary>
        /// 和当前时间比对，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">要对比的时间</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime)
        {
            //如果刷新的时间大于当前时间，说明是提前刷新。算是一天前
            if (beginTime > DateTime.Now) beginTime = beginTime.AddDays(-1);

            return GetPastTimeText(beginTime, DateTime.Now);
        }

        /// <summary>
        /// 开始和结束时间对比，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime, DateTime endTime)
        {
            TimeSpan ts = endTime - beginTime;

            return GetPastTimeText(ts);
        }

        /// <summary>
        /// 获取时长信息(比如说一首歌时长是03:12,3分12秒;一部电影时长01:45:00,1小时45分钟)
        /// </summary>
        /// <param name="secondCount">总秒数</param>
        /// <returns></returns>
        public static string GetDuration(int secondCount)
        {
            if (secondCount < 3600)
            {
                int minute = secondCount / 60;
                int second = secondCount % 60;

                return minute.ToString().PadLeft(2, '0') + ":" + second.ToString().PadLeft(2, '0');
            }
            else
            {
                int hour = secondCount / 3600;
                int tempNum = secondCount - (hour * 3600);
                int minute = tempNum / 60;
                int second = tempNum % 60;
                return hour.ToString().PadLeft(2, '0') + ":" + minute.ToString().PadLeft(2, '0') + ":" + second.ToString().PadLeft(2, '0');
            }

            /*
            GetTime(243)   => 04:03
            GetTime(7841)  => 02:10:41
             */
        }

        /// <summary>
        /// 获取过去的时间内容
        /// </summary>
        /// <param name="ts">时间差</param>
        /// <returns></returns>
        public static string GetPastTimeText(TimeSpan ts)
        {
            if (ts.TotalDays > 365)
            {
                return Math.Round(ts.TotalDays / 365) + "年前";
            }
            else if (ts.TotalDays > 30)
            {
                return Math.Round(ts.TotalDays / 30) + "月前";
            }
            else if (ts.TotalDays > 1)
            {
                return Math.Round(ts.TotalDays) + "天前";
            }
            else if (ts.TotalHours > 1)
            {
                return Math.Round(ts.TotalHours) + "小时前";
            }
            else if (ts.TotalMinutes > 1)
            {
                return Math.Round(ts.TotalMinutes) + "分钟前";
            }
            else
            {
                //return Math.Round(ts.TotalSeconds) + "秒前";
                return "刚刚发表";
            }
        }

        /// <summary>
        /// 获取指定的日期是星期几(中文)
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetDayOfWeekOfCn(DateTime dt)
        {
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return  "星期一";
                case DayOfWeek.Tuesday:
                    return  "星期二";
                case DayOfWeek.Wednesday:
                    return  "星期三";
                case DayOfWeek.Thursday:
                    return  "星期四";
                case DayOfWeek.Friday:
                    return  "星期五";
                case DayOfWeek.Saturday:
                    return  "星期六";
                default: //"Sunday":
                    return "星期日";
            }
        }


        /// <summary>
        /// 获取季度，根据时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int GetQuarterByDateTime(DateTime dateTime)
        {
            return GetQuarterByMonth(dateTime.Month);
        }

        /// <summary>
        /// 获取季度，根据月份
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public static int GetQuarterByMonth(int month)
        {
            return month / 3 + (month % 3 > 0 ? 1 : 0);
        }


        /// <summary>
        /// 转成当天零点的时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime GetDayZeroTime(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day);
        }

        /// <summary>
        /// 转成当天最末尾时间时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime GetDayLastTime(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day,23,59,59);
        }

        #region 得到一周的周一和周日的日期
        
        /// <summary> 
        /// 计算某日起始日期（礼拜一的日期） 
        /// </summary> 
        /// <param name="someDate">该周中任意一天</param> 
        /// <returns>返回礼拜一日期，后面的具体时、分、秒和传入值相等</returns> 
        public static DateTime GetMondayDate(this DateTime someDate)
        {
            int i = someDate.DayOfWeek - DayOfWeek.Monday;
            if (i == -1) i = 6;// i值 > = 0 ，因为枚举原因，Sunday排在最前，此时Sunday-Monday=-1，必须+7=6。 
            TimeSpan ts = new TimeSpan(i, 0, 0, 0);
            return someDate.Subtract(ts);
        }
        /// <summary> 
        /// 计算某日结束日期（礼拜日的日期） 
        /// </summary> 
        /// <param name="someDate">该周中任意一天</param> 
        /// <returns>返回礼拜日日期，后面的具体时、分、秒和传入值相等</returns> 
        public static DateTime GetSundayDate(this DateTime someDate)
        {
            int i = someDate.DayOfWeek - DayOfWeek.Sunday;
            if (i != 0) i = 7 - i;// 因为枚举原因，Sunday排在最前，相减间隔要被7减。 
            TimeSpan ts = new TimeSpan(i, 0, 0, 0);
            return someDate.Add(ts);
        }
        #endregion

        #region 时间戳

        /// <summary>
        /// 时间戳计时开始时间
        /// </summary>
        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// DateTime转换为10位时间戳（单位：秒）
        /// </summary>
        /// <param name="dateTime"> DateTime</param>
        /// <returns>10位时间戳（单位：秒）</returns>
        public static long DateTimeToTimeStamp(DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime() - Jan1st1970).TotalSeconds;
        }

        /// <summary>
        /// DateTime转换为13位时间戳（单位：毫秒）
        /// </summary>
        /// <param name="dateTime"> DateTime</param>
        /// <returns>13位时间戳（单位：毫秒）</returns>
        public static long DateTimeToLongTimeStamp(DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime() - Jan1st1970).TotalMilliseconds;
        }

        /// <summary>
        /// 10位时间戳（单位：秒）转换为DateTime
        /// </summary>
        /// <param name="timeStamp">10位时间戳（单位：秒）</param>
        /// <returns>DateTime</returns>
        public static DateTime TimeStampToDateTime(long timeStamp)
        {
            return Jan1st1970.AddSeconds(timeStamp).ToLocalTime();
        }

        /// <summary>
        /// 13位时间戳（单位：毫秒）转换为DateTime
        /// </summary>
        /// <param name="longTimeStamp">13位时间戳（单位：毫秒）</param>
        /// <returns>DateTime</returns>
        public static DateTime LongTimeStampToDateTime(long longTimeStamp)
        {
            return Jan1st1970.AddMilliseconds(longTimeStamp).ToLocalTime();
        }

        #endregion
    }
}
