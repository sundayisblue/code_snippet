﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BoYuan.Framework.Uitility
{
    /// <summary>
    /// 简易邮件发送类
    /// </summary>
    public class Email
    {
        /// <summary>
        /// 发送email
        /// </summary>
        /// <param name="sendtoEmail">邮件接收者地址</param>
        /// <param name="title">邮件标题</param>
        /// <param name="htmlInfo">邮件内容</param>
        /// <param name="sendfromEmail">发件人email地址 例如："uewang@gmail.com</param>
        /// <param name="sendfromPwd">发件人密码(或者为授权码,二级密码等)</param>
        /// <param name="smtp">发送邮件smtp服务器(用不同的邮箱，stmp定义则不同) 例如："smtp.gmail.com",注意：有的smtp服务器需要在邮箱中设置可以使用smtp服务</param>
        /// <param name="CCAddressList">邮件抄送列表(可显示地址)</param>
        /// <param name="BccAddressList">邮件秘密抄送列表(不显示地址)</param>
        /// <param name="postNum">post端口号 通常都是25</param>
        /// <param name="fromUsername">发件人用户名(默认会从sendfromEmail取值) 例如："uewang@gmail.com"中的uewang</param>
        /// <param name="attachmentList">要上传的附件物理路径集合</param>
        /// <returns></returns>
        public static bool MailSend(string sendtoEmail, string title, string htmlInfo,
            string sendfromEmail, string sendfromPwd, string smtp,
            List<string> CCAddressList = null, List<string> BccAddressList = null,
            int postNum = 25, string fromUsername = "" ,List<string> attachmentList=null)
        {
            //邮件内容
            MailMessage myMail = new MailMessage();
            myMail.From = new MailAddress(sendfromEmail);
            myMail.To.Add(new MailAddress(sendtoEmail));
            myMail.Subject = title;
            myMail.SubjectEncoding = System.Text.Encoding.UTF8;
            myMail.Body = htmlInfo;
            myMail.BodyEncoding = System.Text.Encoding.UTF8;
            myMail.IsBodyHtml = true;
            myMail.Priority = MailPriority.High;
            //附件
            if (attachmentList != null && attachmentList.Count > 0)
            {
                foreach (var attachment in attachmentList)
                {
                    myMail.Attachments.Add(new Attachment(attachment));
                }
            }

            

            if (CCAddressList != null && CCAddressList.Count > 0) //抄送
            {
                foreach (var ea in CCAddressList)
                {
                    myMail.CC.Add(new MailAddress(ea));
                }
            }

            if (BccAddressList != null && BccAddressList.Count > 0) //密抄送
            {
                foreach (var ea in BccAddressList)
                {
                    myMail.Bcc.Add(new MailAddress(ea));
                }
            }

            if (fromUsername.Length == 0) fromUsername = sendfromEmail.Split('@')[0];

            //smtp 服务器
            SmtpClient smtpCt = new SmtpClient();
            smtpCt.UseDefaultCredentials = true;
            smtpCt.Host = smtp;
            smtpCt.Port = postNum;
            smtpCt.Credentials = new System.Net.NetworkCredential(fromUsername, sendfromPwd);
            smtpCt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpCt.EnableSsl = false;
            try
            {
                smtpCt.Send(myMail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /*
         * 在这种实现中要注意这样几个问题： 

        1. System.Net.Mail是.NET 2.0以后的版本中被推荐使用的方法，它解决了一些编码问题和一些安全验证的问题，并且对MailMessage类重写，提供了更多的函数，使得建立MailMessage更加便利。 
        2. System.Net.Mail.MailMessage中，加入SubjectEncoding属性，而且对于To, Cc, Bcc都加入了一些域下的方法，使得可以更便利的实现邮件群发。而且使用了更加面向对象的MailAddress类的对象来表明邮件地址。 
        3.System.Net.Mail中加入SmtpClient类，该类包含了诸如本应用中的一些方法和属性，可以对链接进行安全链接的标记等等 
        4.值得注意的是，一般来讲SMTP(Simple Message Transfer Protocol)使用的端口是25，这也是多数邮件服务期提供的端口，但是gmail却不一样，gmail的最新端口是587，而不是先前的465。 
        5.Gmail的服务器是要求安全链接的，所以一定要指定Sender.EnableSsl ＝ true。 
        6.此外SmtpClient对象中有一个非常重要的方法，要介绍一下，就是SendAsync(), 这个方法已经被重载过了，public void SendAsync(MailMessage message, object userToken)要求使用MailMessage作为发送对象，并且勇userToken指定异步操作时调用的方法。public void SendAsync(string from, string recipients, string subject, string body, object userToken) 可以不用建立MailMessage对象直接发送邮件，userToken与上一个函数相同，public void SendAsyncCancel()用来取消异步操作以发送邮件。
        7.有些邮箱服务器需要开启pop3 smtp 等协议，默认有些是不开通的，需要自己登录邮箱设置。
         */

        /* 调用例子
          Email.MailSend("123456@qq.com", "邮件测试123", "邮件测试内容<b>badaf123</b>",
                    "789@163.com",
                    "密码或者授权码等(可能非登录密码)",
                    "smtp.163.com",
                    BccAddressList:new List<string>() { "456789@qq.com"});
         */
    }
}
